package model;

public class TestCase {

	public static void main(String[] args) {
		new TestCase();
	}
	
	public TestCase(){
		Model testcase = new Model();
		String url1 = "https://www.facebook.com";
		String url2 = "https://www.youtube.com";
		String url3 = "https://www.hotmail.com";
		String url4 = "https://www.google.co.th";
		String url5 = "https://www.stackoverflow.com";
		String url6 = "https://www.bitbucket.org";
		String url7 = "https://www.bitded.com";
		String url8 = "https://www.edmodo.com";
		String url9 = "https://www.joomla.com";
		String url10 = "https://www.regis.ku.ac.th";
		String url11 = "https://login.ku.ac.th";
		String url12 = "https://cs.sci.ku.ac.th";
		
		System.out.println(url1 + " : " + testcase.mod(testcase.Hashing(url1)));
		System.out.println(url2 + " : " + testcase.mod(testcase.Hashing(url2)));
		System.out.println(url3 + " : " + testcase.mod(testcase.Hashing(url3)));
		System.out.println(url4 + " : " + testcase.mod(testcase.Hashing(url4)));
		System.out.println(url5 + " : " + testcase.mod(testcase.Hashing(url5)));
		System.out.println(url6 + " : " + testcase.mod(testcase.Hashing(url6)));
		System.out.println(url7 + " : " + testcase.mod(testcase.Hashing(url7)));
		System.out.println(url8 + " : " + testcase.mod(testcase.Hashing(url8)));
		System.out.println(url9 + " : " + testcase.mod(testcase.Hashing(url9)));
		System.out.println(url10 + " : " + testcase.mod(testcase.Hashing(url10)));
		System.out.println(url11 + " : " + testcase.mod(testcase.Hashing(url11)));
		System.out.println(url12 + " : " + testcase.mod(testcase.Hashing(url12)));
	}
	

}
